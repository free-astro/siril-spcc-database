## Workflow
1. Locate a suitable high resolution graph for your sensor or for your filter
2. Use WebPlotDigitizer https://apps.automeris.io/wpd/ to translate the graph into data points. If it's a one shot colour sensor (OSC) make sure to order your datasets R, G and B
3. Use the "export to plotly" feature to visualize your data to make sure the plotted lines look sane. Go back to WebPlotDigitizer to fix any discrepencies
4. Export your final data as csv
5. Edit the csv to remove the headings, leaving only the data
6. Run the appropriate process python script to produce your JSON. See below for examples of how to invoke
7. Run the appropriate visualize python script to produce a graph based on the JSON
8. Perform a sanity check by comparing the original manufacturer graph with the produced graph to ensure they are visually similar

## process_<type>.py
This is a tool that will parse the csv output from https://apps.automeris.io/wpd/ and generate the appropriate json. Please remove headers from csv before running. If OSC data, make sure you map the columns R, G, & B

### Example usage
```
python process_osc_sensor.py Sony_IMX585C.csv --manufacturer "Sony" --model "IMX585" --dataSource "https://player-one-astronomy.com/product/uranus-c-usb3-0-color-camera-imx585/" > Sony_IMX585C.json
```

## visualize.py
This is a tool to visualize the data in the json. You can then visually compare this to the original graph for sanity checking

### Example usage
```
python visualize_osc_sensor.py Sony_IMX585C.json
```
