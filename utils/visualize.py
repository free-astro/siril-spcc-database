import json
import matplotlib.pyplot as plt
import sys

def get_channel_color(channel):
    """
    Return the appropriate color based on the channel value.
    Args:
        channel (str): Channel identifier (RED, GREEN, BLUE, RGB)
    Returns:
        str: Color name for matplotlib
    """
    color_map = {
        'RED': 'red',
        'GREEN': 'green',
        'BLUE': 'blue',
        'RGB': 'purple'
    }
    return color_map.get(channel.upper(), 'black')

def plot_from_json(filename, output_file=None):
    """
    Create a plot from JSON data file with spectral response curves.
    Each curve is colored according to its channel (RED, GREEN, BLUE, RGB).
    
    Args:
        filename (str): Path to the JSON input file
        output_file (str, optional): Path to save the plot. If None, display the plot
    """
    # Load JSON data from the file
    with open(filename, 'r') as file:
        data = json.load(file)

    # Create figure and axis objects with specific size
    fig, ax = plt.subplots(figsize=(12, 6))

    # Iterate over each data block in the JSON file
    for dataset in data:
        # Extract values from the current data block
        wavelength = dataset['wavelength']['value']
        wavelength_units = dataset['wavelength'].get('units', 'nm')
        values = dataset['values']['value']
        
        # Get the channel and corresponding color
        channel = dataset.get('channel', 'CLEAR')
        color = get_channel_color(channel)

        # Plot the curve with the appropriate color
        # Use only the channel as the label instead of the name
        ax.plot(wavelength, values, label=f"{channel}", color=color)

    # Configure the plot layout
    ax.set_xlabel(f'Wavelength / {wavelength_units}')
    ax.set_ylabel('Transmittance')
    ax.set_title('Spectral Response by Channel')
    ax.grid(True, alpha=0.3)

    # Adjust the layout first
    plt.subplots_adjust(right=0.85)  # Make room for the legend

    # Add legend outside of the plot
    ax.legend(loc='center left', bbox_to_anchor=(1, 0.5))

    # Save or display the plot
    if output_file:
        plt.savefig(output_file, 
                   bbox_inches='tight',
                   dpi=300)
    else:
        plt.show()

if __name__ == "__main__":
    if len(sys.argv) < 2:
        print("Usage: python visualize.py <filename> [output_file]")
        sys.exit(1)

    filename = sys.argv[1]
    output_file = sys.argv[2] if len(sys.argv) > 2 else None

    plot_from_json(filename, output_file)
