### Merge Request Checklists
- [ ] I did read the [README](../../README.md) and the [documentation page](https://siril.readthedocs.io/en/latest/processing/color-calibration/spcc-database.html#converting-the-data) detailing how to form a coherent and useful data set for the database.
- [ ] I checked that there were no duplicates in the file by running the `utils/remove_duplicates` script.
- [ ] I checked the data display using the `utils/visualize.py` script.
- [ ] I've attached the png file of the result of the `utils/visualize.py` script in this MR.
